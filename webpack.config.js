//webpack.config.js
'use strict';

const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const production = ( process.env.NODE_ENV == 'production' );

module.exports = {
    entry: "./src/js/index.js",
    output: {
        path: path.resolve(__dirname, './build/js'),
        filename: "app.min.js"
    },
    mode: process.env.NODE_ENV,
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [".js", ".json"]
    },
    //devtool: 'source-map',
    optimization: {
        minimize: production
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['es2015']
                }
            },
        ]
    },
    plugins: [
        new UglifyJsPlugin({
             uglifyOptions:{
                compress: production ? {warnings: false} : false,
                output: {
                    comments: !production
                },
                sourceMap: production
            }
        })
    ]
};