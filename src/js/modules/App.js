import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
//import WOW from 'wowjs';
import jQuery from 'jquery';

export default class App{

    constructor(){

        ($ => {
            let testDiv = $('.test-el');

            console.log(testDiv.html());
        })(jQuery);



        //let wow = new WOW.WOW().init();

        UIkit.use(Icons);

        // components can be called from the imported UIkit reference

        console.log('test');
        UIkit.notification('Hello world.');

    }
}